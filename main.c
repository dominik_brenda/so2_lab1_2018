#include<grp.h>
#include<pwd.h>
#include<stdio.h>
#include<stdlib.h>
#include<utmp.h>

#include "runtime_options.h"

/**
 *  @brief e_flags	enum
 *  @desc enumerates flags used by program;
 *  last enum value (FLAG_COUNT) is number of flags used;
 *  any flags should be added before FLAG_COUNT
 *  and need to be ordered as corresponding letters in g_flag_list constant
 */
static enum
{
	FLAG_GROUP,
	FLAG_HOST,
	FLAG_COUNT
} e_flags;

/**
 *  @brief g_flag_list	const cstring
 *  @desc all handled options used by the program;
 */
static const char* g_flag_list = "gh";

/**
 *  @brief g_flags	int[]
 *  @desc array storing all flags values
 */
static int g_flags[FLAG_COUNT];

/**
 *  @brief show_user_groups	void
 *  @desc prints all groups that a user is a member of to standard output
 *  @param a_username	char*	username of the user whose groups are to be printed
 */
void show_user_groups(char*);

/**
 *  @brief show_host	void
 *  @desc prints host for a login entry from utmp struct to standard output
 *  @param a_login_record	utmp struct storing login record whose host is to be printed
 */
void show_host(struct utmp*);

/**
 *  @brief show_users	void
 *  @desc prints to a standard output usernames and, if the corresponding flags are set,
 *  host and groups of all currently logged in users
 */
void show_users(void);

int main(int argc, char **argv)
{
	// function reads options from process arguments and sets corresponding flags
	read_options(argc, argv, g_flag_list, FLAG_COUNT, g_flags);
	show_users();
	return 0;
}

void show_user_groups(char *a_username)
{
	// number of groups of the user; set to correct number by first getgrouplist invocation
	int number_of_groups = 0;
	gid_t *group_list = NULL;
	// passwd struct of a user, needed to get user's main group
	struct passwd *pwd = getpwnam(a_username);
	// first invocation sets number_of_groups to correct value
	getgrouplist(a_username, pwd->pw_gid, group_list, &number_of_groups);
	group_list = malloc(sizeof(gid_t) * number_of_groups);
	// second invocation fills gid_t array with user's groups data
	getgrouplist(a_username, pwd->pw_gid, group_list, &number_of_groups);
	printf("[");
	for(int group_counter = 0; group_counter < number_of_groups; ++group_counter)
	{
		char* group_name = getgrgid(group_list[group_counter])->gr_name;
		printf("%s", group_name);
		if(group_counter + 1 < number_of_groups)
		{
			printf(", ");
		}
	}
	printf("]");
	free(group_list);
}

void show_host(struct utmp *a_login_record)
{
	printf("(");
	char* host = a_login_record->ut_host;
	printf("%s)", host);
}

void show_users(void)
{
	for(struct utmp *utmp_entry = getutent(); NULL != utmp_entry; utmp_entry = getutent())
	{
		if(USER_PROCESS == utmp_entry->ut_type)
		{
			printf("%s", utmp_entry->ut_user);	//print username
			if(g_flags[FLAG_HOST])			// -h flag
			{
				printf("\t");
				show_host(utmp_entry);
			}
			if(g_flags[FLAG_GROUP])			// -g flag
			{
				printf("\t");
				show_user_groups(utmp_entry->ut_user);
			}
			printf("\n");
		}
	}
}
