NAME=main
LDIR=lib
OLIB=runtime_options
OPT_LIB=$(LDIR)/$(OLIB)
LINK_OLIB=-L$(LDIR)/$(OLIB) -l$(OLIB) -I $(LDIR)/$(OLIB)/inc

main: lib$(OLIB).a
	@ gcc $(NAME).c $(LINK_OLIB) -o $@

lib$(OLIB).a:
	@ cd $(OPT_LIB) && make

.PHONY: clean

clean: cleanlib
	@ rm -f $(NAME) *.o *.s *.i

.PHONY: cleanlib

cleanlib:
	@ cd $(OPT_LIB) && make clean
